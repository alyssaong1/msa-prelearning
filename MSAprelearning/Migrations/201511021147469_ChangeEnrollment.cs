namespace MSAprelearning.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeEnrollment : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Enrollment", "Date", c => c.String(unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Enrollment", "Date");
        }
    }
}
