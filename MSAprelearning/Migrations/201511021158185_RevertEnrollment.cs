namespace MSAprelearning.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RevertEnrollment : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Enrollment", "Date");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Enrollment", "Date", c => c.String(unicode: false));
        }
    }
}
